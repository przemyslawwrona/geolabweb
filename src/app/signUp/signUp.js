angular.module('geolab.view.signUp', [
    'ui.router',
    'geolab.remote.authRemoteService',
    'geolab.storage.userStorageService',
    'geolab.notifications.appNotificationsService',
    'geolab.utils.userInfoService'
])
    .config(function config($stateProvider) {
        $stateProvider.state('signUp', {
            url: '/register',
            controller: 'SignUpCtrl',
            templateUrl: 'signUp/signUp.tpl.html'
        });
    })

    .controller('SignUpCtrl', ['$scope', 'AuthRemoteService', 'UserRemoteService', 'UserStorageService', 'AppNotificationsService', 'UserInfoService', function ($scope, AuthRemoteService, UserRemoteService, UserStorageService, AppNotificationsService, UserInfoService) {

        function clearUserData() {
            $scope.user = {
                username: "",
                password: "",
                email: "",
                userData: {}
            };
            if($scope.signUpForm) {
                $scope.signUpForm.$setPristine();
            }
        }
        clearUserData();

        $scope.signUp = function () {
            $scope.showLoader = true;
            $scope.authError = undefined;
            UserRemoteService.register($scope.user).then(function (status) {
                $scope.showLoader = false;
                $scope.registerSuccess = true;
                clearUserData();

                //natychmiastowe logowanie:
                //AuthRemoteService.login($scope.credentials.email, $scope.credentials.password).then(function (data) {
                //    UserRemoteService.fetchLoggedUser().then(function() {
                //        $scope.showLoader = false;
                //        AppNotificationsService.loginConfirmed();
                //    });
                //});
            }, function (reason) {
                $scope.showLoader = false;
                UserInfoService.showError(reason.errorMessage);
                $scope.authError = reason.errorMessage;
            });
        };
    }]);