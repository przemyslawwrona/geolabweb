angular.module('geolab.view.project', [
    'ui.router',
    'geolab.model.project',
    'geolab.remote.projectRemoteService'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.project', {
            url: '/project',
            views: {
                'manager@app': {
                    controller: 'ProjectCtrl',
                    templateUrl: 'manager/project/project.tpl.html'
                }
            },
            resolve: {
                projects: ['ProjectRemoteService', function (ProjectRemoteService) {
                    return ProjectRemoteService.getAll();
                }]
            }
        });
    }])
    .controller('ProjectCtrl', ['$scope', '$state', 'ProjectRemoteService', 'Project', 'projects', function ($scope, $state, ProjectRemoteService, Project, projects) {

        $scope.projects = projects;
        $scope.project = Project.build({});

        $scope.opened = [];
        $scope.open = function ($event, indexElementOpened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[indexElementOpened] = !$scope.opened[indexElementOpened];
        };

        $scope.addProject = function(project) {
            ProjectRemoteService.create(project).then(function(result){
                $scope.project.id = result.objectId;
                projects.push($scope.project);
                $scope.project = Project.build({});
            });
        };

        $scope.removeProject = function(index) {
            var removeProject = projects[index];
            ProjectRemoteService.remove(removeProject.id).then(function () {
                $scope.projects.splice(index, 1);
            });
        };

        $scope.saveProject = function(projectId, project) {
            console.log(project);
            ProjectRemoteService.update(projectId, project).then(function(){

            });
        };
    }]);