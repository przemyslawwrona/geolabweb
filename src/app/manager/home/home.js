angular.module('geolab.view.home', [
        'ui.router'
    ])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.home', {
            url: '/',
            views: {
                'manager@app': {
                    controller: 'HomeCtrl',
                    templateUrl: 'manager/home/gravimeterTableModal.tpl.html'
                }
            }
        });
    }])
    .controller('HomeCtrl', ['$scope', '$state', function ($scope, $state) {
        $state.go('app.project');
    }]);