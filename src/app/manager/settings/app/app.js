angular.module('geolab.view.settingsApp', [
    'ui.router'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.settings.app', {
            url: '/app',
            views: {
                'settings@app.settings': {
                    controller: 'SettingsAppCtrl',
                    templateUrl: 'manager/settings/app/app.tpl.html'
                }
            }
        });
    }])
    .controller('SettingsAppCtrl', ['$scope', 'UserRemoteService', 'currentUser', function ($scope, UserRemoteService, currentUser) {

        $scope.userData = {
            username: currentUser.username,
            oldPass: "",
            newPass: ""
        };

        $scope.changePassword = function(userData) {
            UserRemoteService.changePassword(userData).then(function () {
                $scope.userData = {
                    username: currentUser.username,
                    oldPass: "",
                    newPass: ""
                };
            });
        };

    }]);