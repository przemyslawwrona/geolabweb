angular.module('geolab.view.settingsGravimetry', [
    'ui.router'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.settings.gravimetry', {
            url: '/gravimetry',
            views: {
                'settings@app.settings': {
                    controller: 'SettingsGravimetryCtrl',
                    templateUrl: 'manager/settings/gravimetry/gravimetry.tpl.html'
                }
            }
        });
    }])
    .controller('SettingsGravimetryCtrl', ['$scope', 'UserRemoteService', 'currentUser', function ($scope, UserRemoteService, currentUser) {


    }]);