angular.module('geolab.view.settings', [
    'ui.router'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.settings', {
            url: '/settings',
            views: {
                'manager@app': {
                    controller: 'SettingsCtrl',
                    templateUrl: 'manager/settings/settings.tpl.html'
                }
            }
        });
    }])
    .controller('SettingsCtrl', ['$scope', 'UserRemoteService', 'currentUser', function ($scope, UserRemoteService, currentUser) {

    }]);