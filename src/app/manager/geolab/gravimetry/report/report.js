angular.module('geolab.view.reportGravimetry', [
    'ui.router',
    'geolab.remote.gravimetryRemoteService',
    'geolab.directives.openFile',
    'geolab.notifications.modalService',
    'geolab.model.gravReport',
    'geolab.model.gravObservation',
    'geolab.utils.userInfoService',
    'geolab.storage.reportStorageService',
    'xeditable',
    'ui.bootstrap'
])
    .constant("EMPTY_REPORT", {
        report: {
            calibration: {},
            observations: []
        }
    })
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.geolab.gravimetry.report', {
            url: '/report/:reportId',
            views: {
                'gravimetry@app.geolab.gravimetry': {
                    controller: 'ReportGravimetryCtrl',
                    templateUrl: 'manager/geolab/gravimetry/report/report.tpl.html'
                }
            },
            resolve: {
                report: ['$stateParams', 'GravimetryRemoteService', 'ReportStorageService', 'EMPTY_REPORT', function ($stateParams, GravimetryRemoteService, ReportStorageService, EMPTY_REPORT) {
                    if ($stateParams.reportId && ReportStorageService.getReport($stateParams.projectId, $stateParams.reportId)) {
                        console.log('Raport zapisany. WCZYTUJE');
                        return ReportStorageService.getReport($stateParams.projectId, $stateParams.reportId);
                    }
                    if ($stateParams.reportId) {
                        console.log('Raport. POBIERAM');
                        return GravimetryRemoteService.getSavedReport($stateParams.projectId, $stateParams.reportId);
                    }
                    return EMPTY_REPORT;
                }]
            }
        });
    }])
    .controller('ReportGravimetryCtrl', ['$scope', '$stateParams', 'ModalService', 'UserInfoService', 'GravimetryRemoteService', 'GravReport', 'GravObservation', 'ReportStorageService', 'CORRECTIONS', 'lodash', 'ngProgressFactory', 'reports', 'report', function ($scope, $stateParams, ModalService, UserInfoService, GravimetryRemoteService, GravReport, GravObservation, ReportStorageService, CORRECTIONS, lodash, ngProgressFactory, reports, report) {
        $scope.report = report;
        $scope.progressbar = ngProgressFactory.createInstance();


        $scope.calibration = {};

        $scope.corrections = {
            gradient: CORRECTIONS.GRADIENT.NONE,
            luni: CORRECTIONS.LUNI.NONE,
            pressure: CORRECTIONS.PRESSURE.NONE,
            drift: CORRECTIONS.DRIFT.NONE
        };

        $scope.CORRECTIONS_OPTIONS = CORRECTIONS;

        $scope.opened = [];

        $scope.open = function ($event, indexElementOpened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[indexElementOpened] = !$scope.opened[indexElementOpened];
        };

        $scope.showModal = function (calibrationTable) {
            ModalService.calibrationTable(calibrationTable);
        };

        // remove user
        $scope.removeObservation = function (index) {
            $scope.report.observations.splice(index, 1);
        };

        function getGravObservation(lastObservation) {
            return GravObservation.build({
                name: "",
                latitude: lastObservation ? lastObservation.latitude : "",
                longitude: lastObservation ? lastObservation.longitude : "",
                height: lastObservation ? lastObservation.height : "",
                date: lastObservation ? lastObservation.date : "",
                instrumentHeight: "",
                value: "",
                gradient: lastObservation ? lastObservation.gradient : ""
            });
        }

        // add observation
        $scope.addObservation = function () {
            var lastObservation = lodash.last($scope.report.observations);
            var inserted = getGravObservation(lastObservation);
            if (!$scope.report.observations) {
                $scope.report.observations = [];
            }
            $scope.report.observations.push(inserted);
        };

        $scope.getReport = function () {
            $scope.progressbar.start();
            GravimetryRemoteService.getReport($scope.calibration, $scope.corrections, $scope.report).then(function (data) {
                $scope.report = data;
                $scope.progressbar.complete();
            });
        };

        $scope.saveReport = function () {
            GravimetryRemoteService.saveReport($stateParams.projectId, $scope.calibration, $scope.corrections, $scope.report).then(function (report) {
                UserInfoService.showSuccess("Success", "Successfully saved report");
                ReportStorageService.addReport($stateParams.projectId, report);
                reports.push(report);
            }, function () {
                UserInfoService.showError("Error", "Problem with saving report");
            });
        };

        $scope.commaToDot = function (data) {
            return data.indexOf(",") >= 0;
        };
    }]);
