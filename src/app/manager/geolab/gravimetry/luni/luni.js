angular.module('geolab.view.luni', [
    'ui.router',
    'geolab.remote.gravimetryRemoteService',
    'geolab.model.luni',
    'geolab.storage.reportStorageService',
    'geolab.utils.userInfoService',
    'xeditable'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.geolab.gravimetry.luni', {
            url: '/luni/:reportId',
            views: {
                'gravimetry@app.geolab.gravimetry': {
                    controller: 'LuniCtrl',
                    templateUrl: 'manager/geolab/gravimetry/luni/luni.tpl.html'
                }
            },
            resolve: {
                report: ['$stateParams', 'GravimetryRemoteService', function ($stateParams, GravimetryRemoteService) {
                    if ($stateParams.reportId) {
                        return GravimetryRemoteService.getSavedLuni($stateParams.projectId, $stateParams.reportId);
                    }
                }]
            }
        });
    }])
    .controller('LuniCtrl', ['$scope', '$stateParams', 'GravimetryRemoteService', 'ReportStorageService', 'UserInfoService', 'lodash', 'reports', 'report', 'LUNI_CORRECTIONS', 'ngProgressFactory', function ($scope, $stateParams, GravimetryRemoteService, ReportStorageService, UserInfoService, lodash, reports, report, LUNI_CORRECTIONS, ngProgressFactory) {
        $scope.LUNI_CORRECTIONS = LUNI_CORRECTIONS;

        $scope.dateTime = moment().format();
        $scope.interval = 600;

        $scope.position = {
            longitude: 0.0,
            latitude: 0.0,
            altitude: 0.0
        };
        $scope.luniCorrection = LUNI_CORRECTIONS.LONGMAN;

        $scope.report = report;

        $scope.open = function () {
            $scope.isOpen = !$scope.isOpen;
        };

        $scope.getLuni = function (date, position, interval, luniCorrection) {
            GravimetryRemoteService.getLuni(date, position, interval, luniCorrection).then(function (data) {
                $scope.report = data;
            });
        };

        $scope.saveLuni = function (date, position, interval, luniCorrection) {
            GravimetryRemoteService.saveLuni($stateParams.projectId, date, position, interval, luniCorrection).then(function (report) {
                UserInfoService.showSuccess("Success", "Successfully saved report");
                reports.push(report);
                ReportStorageService.addReport($stateParams.projectId, report);
            });
        };

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position.longitude = position.coords.longitude;
                    $scope.position.latitude = position.coords.latitude;
                    $scope.position.altitude = position.coords.altitude ? position.coords.altitude : 0.0;
                });
            });
        } else {
            $scope.position = 'is not support';
        }
    }]);
