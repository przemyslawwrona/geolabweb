angular.module('geolab.view.gravimetry', [
    'ui.router',
    'geolab.remote.gravimetryRemoteService',
    'geolab.directives.openFile',
    'geolab.notifications.modalService',
    'geolab.model.gravReport',
    'geolab.model.gravObservation',
    'xeditable',
    'ui.bootstrap'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.geolab.gravimetry', {
            url: '/gravimetry',
            views: {
                'geolab@app.geolab': {
                    controller: 'GravimetryCtrl',
                    templateUrl: 'manager/geolab/gravimetry/gravimetry.tpl.html'
                }
            }
        });
    }])
    .run(function (editableOptions) {
        editableOptions.theme = 'bs3';
    })
    .controller('GravimetryCtrl', ['$scope', 'ModalService', 'GravimetryRemoteService', 'GravReport', 'GravObservation', 'CORRECTIONS', 'lodash', function ($scope, ModalService, GravimetryRemoteService, GravReport, GravObservation, CORRECTIONS, lodash) {
    }]);
