angular.module('twoostly.view.geolab', [
    'ui.router',
    'geolab.local.geolabService'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.geolab', {
            url: '/project/:projectId',
            views: {
                'manager@app': {
                    controller: 'GeolabCtrl',
                    templateUrl: 'manager/geolab/geolab.tpl.html'
                },
                'slider@app': {
                    controller: 'SliderCtrl',
                    templateUrl: 'manager/geolab/slider/slider.tpl.html'
                }
            },
            resolve: {
                reports: ['$stateParams', 'ReportRemoteService', 'ReportStorageService', function ($stateParams, ReportRemoteService, ReportStorageService) {
                    if(ReportStorageService.getReports($stateParams.projectId)) {
                        console.log('Raporty są zapisane. ZWRACAM');
                        return ReportStorageService.getReports($stateParams.projectId);
                    }
                    return ReportRemoteService.report($stateParams.projectId);
                }]
            }
        });
    }])
    .controller('GeolabCtrl', ['$scope', '$stateParams', 'reports', function ($scope, $stateParams, reports) {
        $scope.isSlidingOpen = true;
    }]);