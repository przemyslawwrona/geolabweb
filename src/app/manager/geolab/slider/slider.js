angular.module('twoostly.view.slier', [
    'ui.router',
    'geolab.remote.reportRemoteService',
    'geolab.utils.userInfoService',
    'geolab.storage.reportStorageService'
])
    .config(['$stateProvider', function config($stateProvider) {
        $stateProvider.state('app.geolab.slider', {
            // url: '/project/:projectId',
            views: {
                'slider@app': {
                    controller: 'SliderCtrl',
                    templateUrl: 'manager/geolab/slider/slider.tpl.html'
                }
            }
        });
    }])
    .controller('SliderCtrl', ['$scope', '$state', '$stateParams', 'ReportRemoteService', 'UserInfoService', 'reports', 'APP_STATES', 'lodash', function ($scope, $state, $stateParams, ReportRemoteService, UserInfoService, reports, APP_STATES, lodash) {
        $scope.reports = reports;
        
        $scope.goToReport = function (report) {
            $state.go(report.object.state, {reportId: report.id});
        };

        $scope.removeReport = function(reportId) {
            var projectId = $stateParams.projectId;
            ReportRemoteService.remove(projectId, reportId).then(function () {
                lodash.remove($scope.reports, function(report) { return report.id == reportId; });
                UserInfoService.showSuccess("Success", "Successfully removed report");
            }, function () {
                UserInfoService.showError("Error", "Problem with saving report");
            });
        };
    }]);