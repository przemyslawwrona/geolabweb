angular.module('geolab.view.manager', [
        'ui.router',
        'geolab.storage.userStorageService',
        'geolab.remote.authRemoteService',
        'geolab.remote.userRemoteService',
        'geolab.notifications.appNotificationsService',
        'geolab.utils.userInfoService'
    ])
    .config(function config($stateProvider) {
        $stateProvider.state('app', {
            url: '',
            views: {
                '@': {
                    controller: 'ManagerCtrl',
                    templateUrl: 'manager/manager.tpl.html'
                },
                'header@app': {
                    controller: 'HeaderCtrl',
                    templateUrl: 'manager/header/header.tpl.html'
                },
                'footer@app': {
                    controller: 'FooterCtrl',
                    templateUrl: 'manager/footer/footer.tpl.html'
                }
            },
            resolve: {
                currentUser: ['UserRemoteService', 'UserStorageService', function (UserRemoteService, UserStorageService) {
                    if (UserStorageService.getCurrentUser()) {
                        return true;
                    }
                    return (UserRemoteService.fetchLoggedUser());
                }]
            }
        });
    })
    .controller('ManagerCtrl', ['$scope', function ($scope) {
    }]);