angular.module('geolab.view.header', [
    'ui.router',
    'geolab.remote.authRemoteService'
])
    .controller('HeaderCtrl', ['$scope', 'AuthRemoteService', function ($scope, AuthRemoteService) {
        $scope.logout = function() {
            AuthRemoteService.logout();
        };
    }]);