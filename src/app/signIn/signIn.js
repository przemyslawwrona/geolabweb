angular.module('geolab.view.signIn', [
    'ui.router',
    'geolab.remote.authRemoteService',
    'geolab.storage.userStorageService',
    'geolab.notifications.appNotificationsService',
    'geolab.utils.userInfoService'
])
    .config(function config($stateProvider) {
        $stateProvider.state('signIn', {
            url: '/signIn',
            controller: 'SignInCtrl',
            templateUrl: 'signIn/signIn.tpl.html'
        }).state('loginConfirmation', {
            url: '/confirmation',
            controller: 'SignInCtrl',
            templateUrl: 'signIn/signIn.tpl.html'
        }).state('loginNewPassword', {
            url: '/login-new-password',
            controller: 'SignInCtrl',
            templateUrl: 'signIn/signIn.tpl.html'
        });
    })

    .controller('SignInCtrl', ['$scope', '$state', 'AuthRemoteService', 'UserRemoteService', 'UserStorageService', 'AppNotificationsService', 'UserInfoService', function ($scope, $state, AuthRemoteService, UserRemoteService, UserStorageService, AppNotificationsService, UserInfoService) {
        var LOGIN_ERROR_MSG = "Incorrect username or password!";

        $scope.isConfirmation = function () {
            return $state.current.name === 'loginConfirmation';
        };

        $scope.isNewPasswordLogin = function () {
            return $state.current.name === 'loginNewPassword';
        };

        $scope.credentials = {
            login:"",
            password:""
        };

        $scope.login = function () {
            $scope.showLoader = true;
            $scope.authError = undefined;
            AuthRemoteService.login($scope.credentials.login, $scope.credentials.password).then(function (data) {
                UserRemoteService.fetchLoggedUser().then(function(){
                    $scope.showLoader = false;
                    AppNotificationsService.loginConfirmed();
                });

            }, function (reason) {
                $scope.showLoader = false;
                UserInfoService.showError(LOGIN_ERROR_MSG);
                $scope.authError = LOGIN_ERROR_MSG;
            });
        };
    }]);