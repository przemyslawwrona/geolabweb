angular.module('geolab.view.signPwd', [
    'ui.router',
    'geolab.remote.userRemoteService',
    'geolab.notifications.appNotificationsService',
    'geolab.utils.userInfoService'
])
    .config(function config($stateProvider) {
        $stateProvider.state('signPwd', {
            url: '/password-reset?value',
            controller: 'SignPwdCtrl',
            templateUrl: 'signPwd/landing.tpl.html'
        });
    })

    .controller('SignPwdCtrl', ['$scope', '$state', '$stateParams', 'UserRemoteService', 'AppNotificationsService', 'UserInfoService', function ($scope, $state, $stateParams, UserRemoteService, AppNotificationsService, UserInfoService) {
        var resetKey = $stateParams.value;
        if(resetKey) {
            $scope.newPassState = true;
        }

        function clearUserData() {
            $scope.user = {
                username: "",
                token: ""
            };
            if($scope.resetForm) {
                $scope.resetForm.$setPristine();
            }
            if($scope.newPassForm) {
                $scope.newPassForm.$setPristine();
            }
        }

        clearUserData();

        $scope.resetPassword = function() {
            $scope.authError = undefined;
            $scope.emailSentSuccessfully = false;
            $scope.showLoader = true;
            UserRemoteService.resetPasswordInit($scope.user).then(function (status) {
                $scope.showLoader = false;
                $scope.emailSentSuccessfully = true;
                clearUserData();
            }, function (reason) {
                $scope.showLoader = false;
                UserInfoService.showError(reason.errorMessage);
                $scope.authError = reason.errorMessage;
            });
        };

        $scope.setNewPassword = function() {
            $scope.authError = undefined;
            $scope.sentSuccessfully = false;
            $scope.showLoader = true;
            $scope.user.token = resetKey;
            UserRemoteService.setNewPassword($scope.user).then(function (status) {
                $scope.showLoader = false;
                $state.go("loginNewPassword");
            }, function (reason) {
                $scope.showLoader = false;
                UserInfoService.showError(reason.errorMessage);
                $scope.authError = reason.errorMessage;
            });
        };
    }]);