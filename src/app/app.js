angular.module('geolab', [
    'templates-app',
    'templates-common',
    'ui.router',
    'ui.bootstrap',

    'geolab.view.landing',
    'geolab.view.gravimetry',
    'geolab.view.reportGravimetry',
    'twoostly.view.slier',
    'geolab.view.luni',
    'geolab.view.project',
    'geolab.utils.userInfoService',
    'geolab.view.signIn',
    'geolab.view.signUp',
    'geolab.view.signPwd',
    'geolab.view.manager',
    'geolab.view.header',
    'geolab.view.home',
    'geolab.view.footer',
    'geolab.view.settings',
    'geolab.view.settingsApp',
    'geolab.view.settingsGravimetry',
    'twoostly.view.geolab',
    'geolab.auth.http-auth-interceptor',
    'geolab.remote.userRemoteService',
    'jcs-autoValidate',
    'ngFileUpload',
    'xeditable',
    'ngLodash',
    'ngProgress'
])

    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$urlMatcherFactoryProvider', '$locationProvider', function config($stateProvider, $urlRouterProvider, $httpProvider, $urlMatcherFactoryProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
        $urlMatcherFactoryProvider.strictMode(false);
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
        $httpProvider.interceptors.push('HttpInterceptor');
    }])

    .run(['$rootScope', function run($rootScope) {
        String.prototype.startsWith = function (str) {
            return this.substring(0, str.length) === str;
        };
    }])

    .constant('STATES', {
        homeState: 'app.home',
        project: 'app.project',
        loginState: 'signIn',
        loginConfirmation: 'loginConfirmation',
        loginNewPassword: 'loginNewPassword',
        registerState: 'signUp',
        signPwd: 'signPwd'
    })

    .controller('AppCtrl', ['$scope', '$rootScope', '$state', 'STATES', 'EVENTS', function ($scope, $rootScope, $state, STATES, EVENTS) {
        var transitionToState = STATES.project;
        var transitionToParams = {};

        $scope.pageTitle = "GeoLAB";

        $rootScope.$on('event:auth-loginRequired', function () {
            $state.go(STATES.loginState);
        });

        $rootScope.$on('event:auth-loginConfirmed', function () {
            $state.go(transitionToState, transitionToParams);
            transitionToState = STATES.homeState;
            transitionToParams = {};
        });

        $rootScope.$on('event:auth-logoutConfirmed', function () {
            transitionToState = STATES.homeState;
            transitionToParams = {};
            $state.go(STATES.loginState, {});
        });

        $rootScope.$on(EVENTS.showInfoBar, function (event, message) {
            //TODO: show info
        });

        $rootScope.$on(EVENTS.loadingStart, function () {
            //TODO: add loading indicator etc.
        });

        $rootScope.$on(EVENTS.loadingStop, function () {
            //TODO: remove loading indicator etc.
        });

        $rootScope.$on('event:changeState-default', function () {
            $state.go(STATES.homeState);
        });

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (toState.name !== STATES.loginState &&
                toState.name !== STATES.loginConfirmation &&
                toState.name !== STATES.loginNewPassword &&
                toState.name !== STATES.registerState &&
                toState.name !== STATES.signPwd) {
                transitionToState = toState.name;
                transitionToParams = toParams;
            }
        });
    }])
;