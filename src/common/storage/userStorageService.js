angular.module('geolab.storage.userStorageService', [
    'geolab.model.user'
])

    .factory('UserStorageService', ['User', function (User) {

        var currentUser;

        return {
            setLoggedUser: function(user) {
                currentUser = user;
            },
            clearLoggedUser: function() {
                currentUser = undefined;
            },
            getCurrentUser: function () {
                return currentUser;
            }
        };
    }]);
