angular.module('geolab.storage.reportStorageService', [
])

    .factory('ReportStorageService', ['lodash', function (lodash) {
        var NONE = -1;
        var reports = [];

        return {
            addReports: function(projectId, reportsList) {
                if(reports[projectId] === undefined) {
                    console.log('Projekt nie ma raportów. ZAPISUJE');
                    reports[projectId] = reportsList;
                }
            },
            getReports: function(projectId) {
                return reports[projectId];
            },
            addReport: function(projectId, report) {
                var reportIndex = lodash.findIndex(reports[projectId], function (o) { return o.id == report.id; });
                if( reportIndex == NONE) {
                    console.log('Raport nie zapisany. DODAJE');
                    reports.push(report);
                }
            },
            getReport: function(projectId, reportId) {
                return lodash.find(reports[projectId], function (report) { return report.id == reportId; });
            }
        };
    }]);
