angular.module('geolab.local.surchargePlanService', [
    'geolab.model.surchargePlan',
    'geolab.remote.surchargePlanRemoteService',
    'geolab.model.surchargeItem',
    'geolab.model.dateRange'
]).
    constant('APPLIED_ON_TYPES', {
        baseRate: 'BASE RATE',
        sellRate: 'SELL RATE',
        surcharge: 'SURCHARGE'
    }).
    factory('SurchargePlanService', ['$q', 'SurchargePlan', 'SurchargePlanRemoteService', 'SurchargeItem', 'APPLIED_ON_TYPES', function ($q, SurchargePlan, SurchargePlanRemoteService, SurchargeItem, APPLIED_ON_TYPES) {
        var surchargePlans = [];
        var lastId = 0;
        var ISO_DATE_FORMAT = "YYYY-MM-DD";

        function findSurchargePlanById(id) {
            return surchargePlans.map(function (surchargePlan) {
                return surchargePlan.id;
            }).indexOf(id);
        }

        function isDateRangesOverlapping(dateRangeA, dateRangeB) {
            var rangeA = moment.range(dateRangeA.dateFrom, dateRangeA.dateTo);
            var rangeB = moment.range(dateRangeB.dateFrom, dateRangeB.dateTo);
            return rangeA.overlaps(rangeB);
        }

        function isSameStartOrEnd(dateRangeA, dateRangeB) {
            return dateRangeA.dateFrom.isSame(dateRangeB.dateFrom, 'day') || dateRangeA.dateFrom.isSame(dateRangeB.dateTo, 'day') || dateRangeA.dateTo.isSame(dateRangeB.dateFrom, 'day') || dateRangeA.dateTo.isSame(dateRangeB.dateTo, 'day');
        }

        return {
            setCurrentSurchargePlans: function (currentSurchargePlans) {
                surchargePlans = currentSurchargePlans;
            },
            createEmptySurchargePlan: function (propertyId) {
                var surchargePlan = SurchargePlan.build({});
                surchargePlan.surchargeItems = [];
                surchargePlan.propertyId = Number(propertyId);
                return surchargePlan;
            },
            addNewSurchargePlan: function (surchargePlan) {
                surchargePlan.dates.forEach(function (dateRange) {
                    dateRange.dateFrom = dateRange.dateFrom.format(ISO_DATE_FORMAT);
                    dateRange.dateTo = dateRange.dateTo.format(ISO_DATE_FORMAT);
                });
                return SurchargePlanRemoteService.saveSurchargePlan(surchargePlan);
            },
            updateSurchargePlan: function (surchargePlan) {
                return SurchargePlanRemoteService.updateSurchargePlan(surchargePlan.id, surchargePlan);
            },
            deleteSurchargePlan: function (surchargePlan) {
                return SurchargePlanRemoteService.deleteSurchargePlan(surchargePlan.id).then(function (response) {
                    var surchargePlanIndex = findSurchargePlanById(surchargePlan.id);
                    surchargePlans.splice(surchargePlanIndex, 1);
                });
            },
            createSurchargeItemFromSurcharge: function (surcharge) {
                var surchargeItem = SurchargeItem.build({});
                surchargeItem.id = lastId++;
                surchargeItem.surcharge = angular.copy(surcharge);

                return surchargeItem;

            },
            isValidDate: function (dateRange, dateRanges) {
                var result = true;
                dateRanges.forEach(function (_dateRange) {
                    if (isDateRangesOverlapping(dateRange, _dateRange) || isSameStartOrEnd(dateRange, _dateRange)) {
                        result = false;
                    }
                });
                return result;
            },
            getAppliedOnTypes: function () {
                return [APPLIED_ON_TYPES.baseRate, APPLIED_ON_TYPES.sellRate, APPLIED_ON_TYPES.surcharge];
            }

        };
    }]);