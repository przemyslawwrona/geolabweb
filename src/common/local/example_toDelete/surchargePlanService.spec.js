//describe('Surcharge Plan Service tests', function () {
//    var SurchargePlanService, DateRange;
//
//    beforeEach(module("cnt_stash.local.surchargePlanService"));
//
//    beforeEach(inject(function (_SurchargePlanService_, _DateRange_) {
//        SurchargePlanService = _SurchargePlanService_;
//        DateRange = _DateRange_;
//
//    }));
//
//    it('empty dates', function () {
//        //given
//        var dateRangeA = DateRange.build({});
//        dateRangeA.dateTo.add(6, 'd');
//
//        //when
//        var dateRanges = [];
//
//        //then
//        expect(SurchargePlanService.isValidDate(dateRangeA, dateRanges)).toEqual(true);
//
//    });
//
//    it('doubled single day', function () {
//        //given
//        var dateRangeA = DateRange.build({});
//        var dateRanges = [];
//
//        //when
//        dateRanges.push(dateRangeA);
//
//        //then
//        expect(SurchargePlanService.isValidDate(dateRangeA, dateRanges)).toEqual(false);
//
//    });
//
//    it('two simple date ranges should not overlap', function () {
//        //given
//        var dateRangeA = DateRange.build({});
//        dateRangeA.dateTo.add(1, 'd');
//
//        var dateRangeB = DateRange.build({});
//        dateRangeB.dateFrom.add(2, 'd');
//        dateRangeB.dateTo.add(4, 'd');
//
//        var dateRanges = [];
//
//        //when
//        dateRanges.push(dateRangeB);
//
//        //then
//        expect(SurchargePlanService.isValidDate(dateRangeA, dateRanges)).toEqual(true);
//
//    });
//
//    it('entirely overlapping ranges [-----] -> [--]', function () {
//        //given
//        var dateRangeA = DateRange.build({});
//        dateRangeA.dateTo.add(6, 'd');
//
//        var dateRangeB = DateRange.build({});
//        dateRangeB.dateFrom.add(2, 'd');
//        dateRangeB.dateTo.add(4, 'd');
//
//        var dateRanges = [];
//
//        //when
//        dateRanges.push(dateRangeB);
//
//        //then
//        expect(SurchargePlanService.isValidDate(dateRangeA, dateRanges)).toEqual(false);
//
//    });
//
//    it('entirely overlapping ranges [--] -> [-----]', function () {
//        //given
//        var dateRangeA = DateRange.build({});
//        dateRangeA.dateTo.add(6, 'd');
//
//        var dateRangeB = DateRange.build({});
//        dateRangeB.dateFrom.add(2, 'd');
//        dateRangeB.dateTo.add(4, 'd');
//
//        var dateRanges = [];
//
//        //when
//        dateRanges.push(dateRangeB);
//
//        //then
//        expect(SurchargePlanService.isValidDate(dateRangeA, dateRanges)).toEqual(false);
//
//    });
//
//    it('partially overlapping ranges - by begining [[--]----]', function () {
//        //given
//        var dateRangeA = DateRange.build({});
//        dateRangeA.dateTo.add(6, 'd');
//
//        var dateRangeB = DateRange.build({});
//        dateRangeB.dateTo.add(4, 'd');
//
//        var dateRanges = [];
//
//        //when
//        dateRanges.push(dateRangeB);
//
//        //then
//        expect(SurchargePlanService.isValidDate(dateRangeA, dateRanges)).toEqual(false);
//
//    });
//
//    it('partially overlapping ranges - by ending [--[----]]', function () {
//        //given
//        var dateRangeA = DateRange.build({});
//        dateRangeA.dateTo.add(6, 'd');
//
//        var dateRangeB = DateRange.build({});
//        dateRangeB.dateFrom.add(2, 'd');
//        dateRangeB.dateTo.add(6, 'd');
//
//        var dateRanges = [];
//
//        //when
//        dateRanges.push(dateRangeB);
//
//        //then
//        expect(SurchargePlanService.isValidDate(dateRangeA, dateRanges)).toEqual(false);
//
//    });
//});