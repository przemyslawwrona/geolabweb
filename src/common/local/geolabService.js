angular.module('geolab.local.geolabService', []).constant('APP_STATES', {
    reportGrav: {
        classType: 'ReportGrav',
        name: 'Gravimetry',
        state: 'app.geolab.gravimetry.report'
    },
    reportLuni: {
        classType: 'ReportLuni',
        name: 'Luni',
        state: 'app.geolab.gravimetry.luni'
    }
}).factory('GeolabService', ['APP_STATES', function (APPLIED_ON_TYPES) {
}]);