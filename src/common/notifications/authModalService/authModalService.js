angular.module('geolab.local.authModalService', []).

    factory('AuthModalService', ['$uibModal', function ($uibModal) {


        return {
            openLoginModal: function (title, confirmation, newLoginPassword) {
                return $uibModal.open({
                    animation: true,
                    templateUrl: 'manager/signInModal/signInModal.tpl.html',
                    controller: 'SignInCtrl',
                    size: 'sm',
                    resolve: {
                        title: function () {
                            return title;
                        },
                        confirmation: function(){
                            return confirmation ? confirmation : false;
                        },
                        newLoginPassword: function(){
                            return newLoginPassword ? newLoginPassword : false;
                        }
                    }
                });
            },
            openRegisterModal: function () {
                return $uibModal.open({
                    animation: true,
                    templateUrl: 'manager/signUpModal/signUpModal.tpl.html',
                    controller: 'SignUpCtrl',
                    size: 'sm'
                });
            },
            openUpgradeModal: function() {
                return $uibModal.open({
                    animation: true,
                    templateUrl: 'manager/upgradeAccountModal/upgradeAccountModal.tpl.html',
                    controller: 'UpgradeCtrl',
                    size: 'sm'
                });
            }
        };
    }]);