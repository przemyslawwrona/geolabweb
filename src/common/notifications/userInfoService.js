angular.module('geolab.utils.userInfoService', [
        'toaster',
        'ngAnimate'
    ])
    .constant('EVENTS', {
        showInfoBar: 'event:show-info-bar',
        loadingStart: 'event:show-info-loading-start',
        loadingStop: 'event:show-info-loading-stop'
    })
    .factory('UserInfoService', ['$rootScope', 'EVENTS', 'toaster', function ($rootScope, EVENTS, toaster) {
        return {
            showError: function (header, body) {
                toaster.pop('error', header, body);
            },
            showInfo: function (header, body) {
                toaster.pop('info', header, body);
            },
            showSuccess: function (header, body) {
                toaster.pop('success', header, body);
            },
            showWarning: function (header, body) {
                toaster.pop('warning', header, body);
            },
            loadingStart: function () {
                $rootScope.$broadcast(EVENTS.loadingStart);
            },
            loadingStop: function () {
                $rootScope.$broadcast(EVENTS.loadingStop);
            }
        };
    }]);