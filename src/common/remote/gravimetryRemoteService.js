angular.module('geolab.remote.gravimetryRemoteService', [
    'geolab.remote.api',
    'geolab.model.gravReport',
    'geolab.model.luni',
    'geolab.model.report'
])
    .factory('GravimetryRemoteService', ['API', '$q', 'Report', 'GravReport', 'Luni', function (API, $q, Report, GravReport, Luni) {
        return {
            getReport: function (calibration, corrections, report) {
                var url = '/reports/gravimetry';
                return API.getAndResolve(url, GravReport, {
                    calibration: calibration,
                    corrections: corrections,
                    report: report
                });
            },
            saveReport: function (projectId, calibration, corrections, report) {
                var url = '/project/' + projectId + '/gravimetry/report';
                return API.postAndResolve(url, Report, {
                    calibration: calibration,
                    corrections: corrections,
                    report: report
                });
            },
            getSavedReport: function (projectId, reportId) {
                var url = '/project/' + projectId + '/reports/' + reportId + '/gravimetry';
                return API.getAndResolve(url, GravReport);
            },
            getLuni: function (date, position, interval, luniCorrection) {
                var url = '/reports/luni';
                return API.getAndResolve(url, Luni, {date: date, position: position, interval: interval, luniCorrection: luniCorrection});
            },
            saveLuni: function(projectId, date, position, interval, luniCorrection) {
                var url = '/project/' + projectId + '/gravimetry/luni';
                return API.postAndResolve(url, Report, {date: date, position: position, interval: interval, luniCorrection: luniCorrection});
            },
            getSavedLuni: function (projectId, reportId) {
                var url = '/project/' + projectId + '/reports/' + reportId + '/luni';
                return API.getAndResolve(url, Luni);
            }
        };
    }]);
