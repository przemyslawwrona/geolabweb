describe('Auth Remote Service tests', function () {
    var AuthRemoteService, UserAuthService, $httpBackend;


    beforeEach(module("geolab.remote.authRemoteService"));

    beforeEach(inject(function (_AuthRemoteService_, _UserAuthService_,_$httpBackend_) {
        AuthRemoteService = _AuthRemoteService_;
        UserAuthService = _UserAuthService_;
        $httpBackend = _$httpBackend_;
    }));

    it('should login and add authentication headers', function () {
        //given
        $httpBackend.expectPOST('api/login', {username:'user', password:'password'}).respond(200, {access_token: 'token'});

        //when
        AuthRemoteService.login('user','password');
        $httpBackend.flush();

        //then
        expect(UserAuthService.getAuthHeaders()).toEqual({'X-Auth-Token': 'token'});
    });


    it('should logout and remove authentication headers', function () {
        //given
        UserAuthService.setXAuthToken('token');
        $httpBackend.expectPOST('api/logout').respond(200, 'OK');

        //when
        AuthRemoteService.logout();
        $httpBackend.flush();

        //then
        expect(UserAuthService.getAuthHeaders()).toEqual({'X-Auth-Token': ''});
    });


});