
angular.module('geolab.remote.authRemoteService', [
    'geolab.auth.userAuthService',
    'geolab.remote.api',
    'geolab.storage.userStorageService',
    'geolab.notifications.appNotificationsService'
])
    .factory('AuthRemoteService', ['UserAuthService', 'API',  'UserStorageService', 'AppNotificationsService', function (UserAuthService, API, UserStorageService, AppNotificationsService) {
        this.login = function (username, password) {
            return API
                .postUnAuth('/login', {username: username, password: password})
                .then(function (response) {
                    UserStorageService.clearLoggedUser();
                    UserAuthService.setXAuthToken(response.access_token);
                });
        };

        this.logout = function () {
            return API
                .postUnAuth('/logout')
                .then(function (response) {
                    UserStorageService.clearLoggedUser();
                    UserAuthService.removeXAuthToken();
                    AppNotificationsService.logoutConfirmed();
                });
        };
        return this;
    }])
;