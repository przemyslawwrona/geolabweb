angular.module('geolab.remote.userRemoteService', [
    'geolab.remote.api',
    'geolab.model.responseStatus',
    'geolab.model.user',
    'geolab.storage.userStorageService'
])
    .factory('UserRemoteService', ['$rootScope', 'API', '$q', 'ResponseStatus', 'User', 'UserStorageService', function ($rootScope, API, $q, ResponseStatus, User, UserStorageService) {
        return {
            register: function (userData) {
                var url = '/register';
                return API.postBaseAPIWithStatus(url, userData);
            },
            resetPasswordInit: function (userData) {
                var url = '/password/initReset';
                return API.postBaseAPIWithStatus(url, userData);
            },
            setNewPassword: function (userData) {
                var url = '/password/new';
                return API.postBaseAPIWithStatus(url, userData);
            },
            changePassword: function (userData) {
                var url = '/password/change';
                return API.postWithStatus(url, {oldPass: userData.oldPass, newPass: userData.newPass});
            },
            removeUser: function (userId) {
                var url = '/users/' + userId;
                return API.removeWithStatus(url);
            },
            updateUser: function (user) {
                var url = '/users/' + user.id;
                return API.postWithStatus(url, user);
            },
            getUser: function (userId) {
                var url = '/users/' + userId;
                return API.getAndResolve(url, User);
            },
            fetchLoggedUser: function () {
                var deferred = $q.defer();
                API.get('/loggedUser')
                    .then(User.apiResponseTransformer)
                    .then(function (user) {
                        UserStorageService.setLoggedUser(user);
                        deferred.resolve(user);
                    }, function () {
                        deferred.reject();
                    });
                return deferred.promise;
            }
        };

    }]);
