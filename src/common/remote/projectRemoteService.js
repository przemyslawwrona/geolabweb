angular.module('geolab.remote.projectRemoteService', [
    'geolab.remote.api',
    'geolab.model.project'
])
    .factory('ProjectRemoteService', ['API', '$q', 'Project', function (API, $q, Project) {
        return {
            create: function (project) {
                var url = '/project';
                return API.postWithStatus(url, {project: project});
            },
            get: function (id) {
                var url = '/project/' + id;
                return API.getAndResolve(url, GravReport, {id: id});
            },
            getAll: function() {
                var url = "/project";
                return API.getAndResolve(url, Project);
            },
            remove: function (id) {
                var url = '/project/' + id;
                return API.removeWithStatus(url);
            },
            update: function(projectId, project) {
                var url = '/project/' + projectId;
                return API.postWithStatus(url, {project: project});
            }
        };
    }]);
