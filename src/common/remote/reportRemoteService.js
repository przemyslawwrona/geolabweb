angular.module('geolab.remote.reportRemoteService', [
    'geolab.remote.api',
    'geolab.model.gravReport',
    'geolab.model.report'
])
    .factory('ReportRemoteService', ['API', '$q', 'Report', function (API, $q, Report) {
        return {
            report: function (projectId) {
                var url = '/project/' + projectId + '/reports';
                return API.getAndResolve(url, Report);
            },
            remove: function(projectId, reportId) {
                var url = '/project/' + projectId + '/reports/' + reportId;
                return API.removeWithStatus(url);
            }
        };
    }]);
