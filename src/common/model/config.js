angular.module('geolab.model.config', [
    'geolab.model.gravObservation'
])
    .factory('Config', ['CORRECTIONS', function(CORRECTIONS) {

        function Config(id, dateCreated, lastUpdated, gradient, luni, pressure, dryft) {
            this.id = id ? id : "";
            this.dateCreated = dateCreated ? moment(dateCreated).format(): moment().format();
            this.lastUpdated = lastUpdated;
            this.gradient = gradient;
            this.luni = luni;
            this.pressure = pressure;
            this.dryft = dryft;
        }

        Config.build = function(data) {
            return new Config(
                data.id,
                data.dateCreated,
                data.lastUpdated,
                data.gradient,
                data.luni,
                data.pressure,
                data.dryft
            );
        };

        Config.apiResponseTransformer = function(responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(Config.build);
            }
            return Config.build(responseData);
        };

        return Config;
    }]);