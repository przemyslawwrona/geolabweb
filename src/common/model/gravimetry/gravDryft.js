angular.module('geolab.model.gravDrift', [
    'geolab.model.gravObservationDryft'
])
    .factory('GravDrift', ['GravObservationDryft', function (GravObservationDryft) {

        function GravDrift(observations, dryft) {
            this.observations = observations ? GravObservationDryft.apiResponseTransformer(observations) : [];
            this.dryft = dryft;
        }

        GravDrift.build = function (data) {
            return new GravDrift(
                data.observations,
                data.dryft
            );
        };

        GravDrift.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(GravDrift.build);
            }
            return GravDrift.build(responseData);
        };

        return GravDrift;
    }]);