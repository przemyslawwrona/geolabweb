angular.module('geolab.model.gravObservationDryft', [])
    .factory('GravObservationDryft', [function () {

        function GravObservationDryft(name, gRef, dryft, gRefPopra, deltaG) {
            this.name = name;
            this.gRef = gRef;
            this.dryft = dryft;
            this.gRefPopra = gRefPopra;
            this.deltaG = deltaG;
        }

        GravObservationDryft.build = function (data) {
            return new GravObservationDryft(
                data.name,
                data.gRef,
                data.dryft,
                data.gRefPopra,
                data.deltaG
            );
        };

        GravObservationDryft.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(GravObservationDryft.build);
            }
            return GravObservationDryft.build(responseData);
        };

        return GravObservationDryft;
    }]);