angular.module('geolab.model.gravObservation', [])
    .constant("CORRECTIONS", {
        "GRADIENT": {
            "NONE": "0",
            "STANDARD": "1"
        },
        "LUNI": {
            "NONE": "0",
            "LONGMAN": "1",
            "WENZEL": "2"
        },
        "PRESSURE": {
            "NONE": "0",
            "STANDARD": "1"
        },
        "DRIFT": {
            "NONE": "0",
            "LINIOWY": "1",
            "LINIOWY_WAZONY": "2"
        }
    })
    .factory('GravObservation', [function () {

        function GravObservation(name, latitude, longitude, height, date, value, instrumentHeight, gradient, pressure, g, corrections, gRef) {
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.height = height;
            this.date = date;
            this.time = date;
            this.value = value;
            this.instrumentHeight = instrumentHeight;
            this.gradient = gradient;
            this.pressure = pressure;
            this.g = g;

            this.poprawkaLuni = corrections ? (corrections.luni ? corrections.luni : 0.0) : 0.0;
            this.poprawkaGradient = corrections ? (corrections.gradient ? corrections.gradient : 0.0) : 0.0;

            this.gRef = gRef;
        }

        GravObservation.build = function (data) {
            return new GravObservation(
                data.name,
                data.latitude,
                data.longitude,
                data.height,
                data.date,
                data.value,
                data.instrumentHeight,
                data.gradient,
                data.pressure,
                data.g,
                data.corrections,
                data.gRef
            );
        };

        GravObservation.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(GravObservation.build);
            }
            return GravObservation.build(responseData);
        };

        return GravObservation;
    }]);