angular.module('geolab.model.gravReport', [
    'geolab.model.gravObservation',
    'geolab.model.gravDrift'
])
    .factory('GravReport', ['GravObservation', 'GravDrift', function (GravObservation, GravDrift) {

        function GravReport(gravimeter, observations, corrections, dryfts, deltaG) {
            this.gravimeter = gravimeter;
            this.observations = observations ? GravObservation.apiResponseTransformer(observations) : [];
            this.corrections = corrections;
            this.dryfts = dryfts ? GravDrift.apiResponseTransformer(dryfts) : [];
            this.deltaG = deltaG;
        }

        GravReport.build = function (data) {
            return new GravReport(
                data.gravimeter,
                data.observations,
                data.corrections,
                data.dryfts,
                data.deltaG
            );
        };

        GravReport.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(GravReport.build);
            }
            return GravReport.build(responseData);
        };

        return GravReport;
    }]);