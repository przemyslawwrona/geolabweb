angular.module('geolab.model.luni', [])
    .constant("LUNI_CORRECTIONS", {
        "NONE": "0",
        "LONGMAN": "1",
        "WENZEL": "2"

    })
    .factory('Luni', [function () {

        function Luni(date, value) {
            this.date = moment(date).format();
            this.value = value;
        }

        Luni.build = function (data) {
            return new Luni(
                data.date,
                data.value
            );
        };

        Luni.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(Luni.build);
            }
            return Luni.build(responseData);
        };

        return Luni;
    }]);