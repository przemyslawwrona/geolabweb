angular.module('geolab.model.user', [
    'geolab.model.userData'
])
    .factory('User', ['UserData', function(UserData) {

        function User(id, email, username, userData) {
            this.id = id;
            this.username = username;
            this.email = email;
            this.userData = userData;
        }

        User.build = function(data) {
            return new User(
                data.id,
                data.username,
                data.email,
                data.userData ? UserData.build(data.userData) : {}
            );
        };

        User.apiResponseTransformer = function(responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(User.build);
            }
            return User.build(responseData);
        };

        return User;
    }]);