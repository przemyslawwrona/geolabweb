angular.module('geolab.model.userData', [
    'geolab.model.config'
])
    .factory('UserData', ['Config', function(Config) {

        function UserData(id, name, dateCreated, config) {
            this.id = id;
            this.name = name;
            this.dateCreated = moment(dateCreated);
            this.config = Config.build(config);
        }

        UserData.build = function(data) {
            return new UserData(
                data.id,
                data.name,
                data.dateCreated,
                data.config
            );
        };

        UserData.apiResponseTransformer = function(responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(UserData.build);
            }
            return UserData.build(responseData);
        };

        return UserData;
    }]);