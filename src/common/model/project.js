angular.module('geolab.model.project', [
])
    .factory('Project', [function() {

        function Project(id, dateCreated, lastUpdated, title, localization, description) {
            this.id = id ? id : "";
            this.dateCreated = dateCreated ? moment(dateCreated).format(): moment().format();
            this.lastUpdated = lastUpdated;
            this.title = title ? title : "";
            this.localization = localization ? localization : "";
            this.description = description ? description : "";
        }

        Project.build = function(data) {
            return new Project(
                data.id,
                data.dateCreated,
                data.lastUpdated,
                data.title,
                data.localization,
                data.description
            );
        };

        Project.apiResponseTransformer = function(responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(Project.build);
            }
            return Project.build(responseData);
        };

        return Project;
    }]);