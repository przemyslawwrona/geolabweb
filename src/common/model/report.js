angular.module('geolab.model.report', [
    'geolab.local.geolabService'
])
    .factory('Report', ['lodash', 'APP_STATES', function (lodash, APP_STATES) {

        function Report(id, dateCreated, lastUpdated, classType) {
            this.id = id ? id : "";
            this.dateCreated = dateCreated ? moment(dateCreated).format() : moment().format();
            this.lastUpdated = lastUpdated ? moment(lastUpdated).format() : moment().format();
            this.object = Report.assignState(classType);
        }

        Report.assignState = function (classType) {
            return lodash.find(APP_STATES, function(o){return o.classType == classType;});
        };

        Report.build = function (data) {
            return new Report(
                data.id,
                data.dateCreated,
                data.lastUpdated,
                data.classType
            );
        };

        Report.apiResponseTransformer = function (responseData) {
            if (angular.isArray(responseData)) {
                return responseData.map(Report.build);
            }
            return Report.build(responseData);
        };

        return Report;
    }]);