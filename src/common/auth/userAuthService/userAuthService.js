angular.module('geolab.auth.userAuthService', [
    'ipCookie'
])
    .factory('UserAuthService', ['ipCookie', function (ipCookie) {
        var X_AUTH_TOKEN = 'X-Auth-Token';
        var headers = {'X-Auth-Token': getXAuthToken()};

        function saveXAuthToken(xAuthToken) {
            ipCookie(X_AUTH_TOKEN, xAuthToken, {path:"https://localhost:8443"});
        }

        function getXAuthToken() {
            return ipCookie(X_AUTH_TOKEN);
        }

        return {
            setXAuthToken: function (xAuthToken) {
                headers[X_AUTH_TOKEN] = xAuthToken;
                saveXAuthToken(xAuthToken);
            },

            removeXAuthToken: function () {
                headers[X_AUTH_TOKEN] = '';
                saveXAuthToken('');
            },
            getAuthHeaders: function () {
                return headers;
            }
        };
    }])
;

