describe('User Auth Service tests', function () {
    var UserAuthService;


    beforeEach(module('geolab.auth.userAuthService'));

    beforeEach(inject(function (_UserAuthService_) {
        UserAuthService = _UserAuthService_;
    }));


    it('should save auth token', function () {
        //given
        var token = "test_token";

        //when
        UserAuthService.setXAuthToken(token);

        //then
        expect(UserAuthService.getAuthHeaders()).toEqual({'X-Auth-Token': token});
    });

    it('should remove auth token', function () {
        //given
        var token = "test_token";
        UserAuthService.setXAuthToken(token);

        //when
        UserAuthService.removeXAuthToken();

        //then
        expect(UserAuthService.getAuthHeaders()).toEqual({'X-Auth-Token': ''});
    });


});