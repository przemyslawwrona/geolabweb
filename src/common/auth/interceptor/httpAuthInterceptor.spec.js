describe('Http interceptor tests', function () {
    var rootScope, $http, $httpBackend;
    var TEST_URL = "http://test.pl/test";

    beforeEach(module("geolab.auth.http-auth-interceptor"));

    beforeEach(inject(function ($rootScope, _$http_, _$httpBackend_) {
        $http = _$http_;
        $httpBackend = _$httpBackend_;
        rootScope = $rootScope;
        spyOn(rootScope, '$broadcast').andCallThrough();

    }));


    it('should broadcast auth-required when unauthenticated response', function () {
        //given
        $httpBackend.expectGET(TEST_URL).respond(401, {});

        //when
        $http.get(TEST_URL);
        $httpBackend.flush();

        //then
        expect(rootScope.$broadcast).toHaveBeenCalledWith('event:auth-loginRequired');
    });

    it('should not broadcast auth-required when 200 OK response', function () {
        //given
        $httpBackend.expectGET(TEST_URL).respond(200, {});

        //when
        $http.get(TEST_URL);
        $httpBackend.flush();

        //then
        expect(rootScope.$broadcast).not.toHaveBeenCalledWith('event:auth-loginRequired');
    });


});