angular.module('geolab.auth.http-auth-interceptor', [
    'geolab.notifications.appNotificationsService',
    'geolab.auth.userAuthService',
    'geolab.utils.userInfoService',
    'ui.router'
])

    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('HttpInterceptor');
    }])
    .factory('HttpInterceptor', ['$q', '$injector', 'UserAuthService', 'AppNotificationsService', 'UserInfoService', function ($q, $injector, UserAuthService, AppNotificationsService, UserInfoService) {

        var LOGIN_STATE = 'signIn';

        function handleUnauthorized() {
            UserAuthService.removeXAuthToken();
            AppNotificationsService.loginRequired();
            return $q.defer().promise;
        }

        function handleInternalServerError() {
            UserInfoService.showError("Internal Error", "Please contact system administrator");
        }

        function handleFrobidden(){
            UserInfoService.showError("Access Denided", "You have insufficient privileges");
        }

        function handleError(rejection){
            var currentState = $injector.get('$state').current.name;
            if (rejection.status === 401 && currentState !== LOGIN_STATE) {
                handleUnauthorized();
            } else if(rejection.status === 403){
                handleFrobidden();
            } else if(rejection.status === 500){
                handleInternalServerError();
            }
            return $q.reject(rejection);
        }

        return {
            responseError: handleError
        };
    }]);