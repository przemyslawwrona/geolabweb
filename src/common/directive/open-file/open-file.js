angular.module('geolab.directives.openFile', [])
    .constant('FILE_TYPE', {
        json: 'json',
        txt: 'txt'
    })
    .directive('openFile', [function () {
        return {
            restrict: 'A',
            scope: {
                data: "=",
                errorMessage: "="
            },
            //         template: '<input type="file" ng-model="file"><i class="fa fa-folder-open-o fa-4x" aria-hidden="true"></i></input>',
            controller: "OpenFileCtrl"
        };
    }])
    .controller('OpenFileCtrl', ['$scope', '$element', function ($scope, $element) {
        var TWO_MB = 2097152;

        var applyErrorMessage = function (errorMessage) {
            $scope.errorMessage = errorMessage;
        };

        $element.bind("change", function (changeEvent) {
            var reader = new FileReader();
            reader.onload = function (loadEvent) {
                $scope.$apply(function () {
                    var report = JSON.parse(loadEvent.target.result);
                    for (var index in report.observations) {
                        var observation = report.observations[index];
                        observation.date = moment(observation.date).toDate();
                        observation.time = moment(observation.date).toDate();
                    }
                    $scope.data = report;
                });

            };
            reader.readAsText(changeEvent.target.files[0]);
            // reader.readAsDataURL(changeEvent.target.files[0]);
        });


        //     return {
        //         restrict: 'EA',
        //         scope: {
        //             data: "=",
        //             type: "@",
        //             text: "@"
        //         },
        //         template: '<input type="file" ng-model="file"><i class="fa fa-folder-open-o fa-4x" aria-hidden="true"></i></input>',
        //         controller: "OpenFileCtrl"
        //     };
        // }])
        // .controller('OpenFileCtrl', ['$scope', 'FILE_TYPE', function ($scope, FILE_TYPE) {
        //     $scope.file = undefined;
        //     $scope.$watch("file", function (file) {
        //         if (file) {
        //             var fileReader = new FileReader();
        //             fileReader.onload = function (loadEvent) {
        //                 var data = JSON.parse(loadEvent.target.result);
        //                 $scope.data = parseGravimeterRapoet(data);
        //             };
        //             fileReader.readAsText(file);
        //
        //             if (file.size > TWO_MB) {
        //                 applyErrorMessage("Your file it too large!");
        //             }
        //             //else if (!file.type.match(/image\/.*/)) {
        //             //    applyErrorMessage("File you are trying to upload is not a photo!");
        //             //} else {
        //             //    fileReader.readAsDataURL(file);
        //             //}
        //         }
        //     });
    }]);